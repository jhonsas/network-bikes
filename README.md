# Network Bikes
**Spanish:**: Este es un proyecto creado con la finalidad de aprender a usar: Node.js, Express y MongoDB<br>
**English:**: this is a project created with the purpose of learning to use: Node.js, Express and Mongodb<br>

# Tools 🔧
>Express
- npm install express-generator -g

>Node
- npm i nodemon --save-dev

>Mongoose
- npm i mongoose --save
- npm i mongoose-unique-validator --save

>Mailer
- npm install mailer --save

## Others tools
>Testing
- npm i jasmine --save-dev
- npm i -g jasmine
- jasmine init

>Testing API
- npm i request -save

>Moment (Dates)
- npm i moment -save

>Map
- [Leaflet](https://leafletjs.com)
- [OpenStreetMap](https://www.openstreetmap.org)

>Bcrypt
- npm i bcrypt --save