const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], 
        // El indice nos permite un busqueda optima de información, en este caso el type hace referencia a un dato geografico.
        index: { type: '2disphere', sparse: true }
    }
});

BicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
}

BicicletaSchema.method.toString = function () {
    return 'code: ' + this.code + ' | color: ' + this.color;
};

// Busca las bicis en la bd por el Schema
BicicletaSchema.statics.allBicis = function (cb) {
    return this.find({}, cb);
};

BicicletaSchema.statics.add = function (bike, cb) {
    this.create(bike, cb);
};

BicicletaSchema.statics.findByCode = function (code, cb) {
    return this.findOne({code: code}, cb);
};

BicicletaSchema.statics.removeByCode = function (code, cb) {
    return this.deleteOne({code: code}, cb);
};

module.exports = mongoose.model('Bicicleta', BicicletaSchema);

// var Bicicleta = function (id, color, modelo, ubicacion) {
//     this.id = id;
//     this.color = color;
//     this.modelo = modelo;
//     this.ubicacion = ubicacion;
// }

// Bicicleta.prototype.toString = () => {
//     return (    'id: ' + this.id 
//                 + " | color: " + this.color 
//                 + " | modelo: " + this.modelo 
//                 + " | ubicación: " + this.ubicacion
//             );
// }

// Bicicleta.allBicis = [];

// Bicicleta.add = (newBike) => {
//     Bicicleta.allBicis.push(newBike);
// }

// Bicicleta.findById = (biciId) => {
//     const bike = Bicicleta.allBicis.find(bici => bici.id == biciId);
//     if (bike) {
//         return bike;
//     } else {
//         throw new Error(`No existe una bicicleta con el id ${biciId}`);
//     }
// }

// Bicicleta.deleteById = (biciId) => {
//     Bicicleta.findById(biciId);
//     Bicicleta.allBicis.forEach((bici, index, list) => {
//         if (bici.id == biciId) {
//             list.splice(index, 1);
//         }
//     });
// }

// var bike1 = new Bicicleta(1, "rojo", "urbana", [-34.6012424, -58.3861497]);
// var bike2 = new Bicicleta(2, "blanca", "urbana", [-34.596932, -58.3808287]);

// Bicicleta.add(bike1);
// Bicicleta.add(bike2);

// module.exports = Bicicleta;