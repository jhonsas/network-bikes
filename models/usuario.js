const mongoose = require('mongoose');
const Reserva = require('./reserva');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const saltRounds = 10;
const uniqueValidator = require('mongoose-unique-validator');

const Token = require('../models/token');
const mailer = require('../mailer/mailer');

const validateEmail = (email) => {
    const re = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    // .test metodo de regex
    return re.test(email);
}

const usuarioSchema = new Schema({
    nombre: { 
        type: String, 
        trim: true, 
        required: [true, 'El nombre es obligatorio'] 
    },
    email: {
        type: String, 
        trim: true,
        required: [true, 'El nombre es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un email valido'],
        match: [/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/]
    },
    password: {
        type: String,
        required: [true, 'Password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, { message: 'El correo {PATH} ya esta asociado a una cuenta'});

// Función que se ejecutara siempre antes de guardar el usuario
usuarioSchema.pre('save', function(next){
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.method.validatePassword = function (password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reservar = function (bikeID, desde, hasta, cb) {
    let reserva = new Reserva({ usuario: this._id, bicicleta: bikeID, desde: desde, hasta: hasta });
    console.log(reserva);
    reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save((err) => {
        if (err) { return console.log(err.message); }

        const mailOptions = {
            from: 'no-reply@redBicicletas.com',
            to: email_destination,
            subject: 'Verificación de cuenta',
            text: 'Hola,\n\n' + 'Por favor verificar su cuenta haga clic en el siguiente link: \n' + 'https://localhost:3000' + '\/token/confirmation\/' + token.token + '.\n'
        };

        // mailer.sendMail(mailOptions, (err) => {
        //     if (err) { return console.log(err.message); }

        //     console.log('A verification email has been sent to ' + email_destination);
        // });
    });
}

module.exports = mongoose.model('Usuario', usuarioSchema);