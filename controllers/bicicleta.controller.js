const Bicicleta = require("../models/bicicletas");

exports.bicicleta_list = (req, res) => {
    res.render('bicicletas/index', { bicis: Bicicleta.allBicis });
}

exports.bicicleta_create_get = (req, res) => {
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = (req, res) => {
    const bici = new Bicicleta(req.body.id,
                                 req.body.color, 
                                 req.body.modelo, 
                                 [req.body.lat, req.body.lng]
                            );
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}

exports.bicicleta_update_get = (req, res) => {
    const bici = Bicicleta.findById(req.params.id);

    res.render('bicicletas/update', {bici});
}

exports.bicicleta_update_post = (req, res) => {
    const bike = Bicicleta.findById(req.params.id);
    bike.id = req.body.id;
    bike.color = req.body.color;
    bike.modelo = req.body.modelo;
    bike.ubicacion = [req.body.lat, req.body.lng];
    res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = (req, res) => {
    Bicicleta.deleteById(req.body.id);

    res.redirect('/bicicletas');
}