const Usuario = require('../models/usuario');
const Token = require('../models/token');

module.exports = {
    confirmationGet: function (req, res, next) {
        Token.findOne({ token: req.param.token }, (err, token) => {
            if (!token) return res.status(400).send({ type: 'no-verified', msg: 'No se verifico el usuario' });
            Usuario.findById(token._userId, (err, usuario) => {
                if (!usuario) return res.status(400).send({ msg: ' No encontramos un usuario asociado' });
                if (usuario.verificado) return res.redirect('/usuarios');
                usuario.verificado = true;
                usuario.save((err) => {
                    if (err) { return res.status(500).send({ msg: err.message }); }
                    res.redirect('/');
                });
            });
        });
    }
}