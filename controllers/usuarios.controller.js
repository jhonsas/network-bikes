const Usuario = require('../models/usuario');
const { update } = require('../models/reserva');

module.exports = {
    list: function (req, res, next) {
        Usuario.find({}, (err, usuarios) => {
            res.render('users/index', { usuarios: usuarios });
        });
    },
    update_get: function (req, res, next) {
        console.log(JSON.stringify(req.params))
        Usuario.findById({ _id: req.params.id }, (err, usuario) => {
            res.render('users/update', { errors: {}, usuario: usuario });
        });
    },
    update: function (req, res, next) { 
        const update_values = {nombre: req.body.nombre};
        Usuario.findByIdAndUpdate(req.params.id, update_values, (err, usuario) => {
            if(err) {
                console.log(err);
                res.render('users/update', {errors: err.errors, usuario: new Usuario()});
            } else {
                res.redirect('/users');
                return;
            }
        });
    },
    create_get: function (req, res, next) {
        res.render('users/create', {errors:{}, usuario: new Usuario()});
    },
    create: function (req, res, next) {
        if (req.body.password != req.body.confirm_password) {
            res.render('users/create', {errors: {confirm_password: { message: 'No son iguales las claves'}}, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            return;
        }

        Usuario.create({nombre: req.body.nombre, email: req.body.email, password: req.body.password}, (err, nuevoUsuario) => {
            if(err) {
                res.render('users/create', {errors: err.errors, usuario: new Usuario()});
            } else {
                nuevoUsuario.enviar_email_bienvenida();
                res.redirect('/users');
            }
        });
    },
    delete: function (req, res, next) {
        Usuario.findByIdAndDelete(req.body.id, (err) => {
            if(err) {
                next(err);
            } else {
                res.redirect('/users');
            }
        });
    }
}


exports.usuarios_list = function (req, res) {
    Usuario.find({}, () => {
        res.status(200).json({
            usuarios: usuarios
        });
    });
}

exports.usuarios_create = function (req, res) {
    let usuario = new Usuario({ nombre: req.body.nombre });

    usuario.save(() => {
        res.status(200).json(usuario);
    });
}

exports.usuario_reservar = function (req, res) {
    Usuario.findById(req, body.id, function (err, usuario) {
        console.log(usuario);
        usuario.reservar(req.body.bike_id, req.body.desde, req.body.hasta, (err) => {
            console.log('reserva ||||');
            res.status(200).send();
        });
    })
}