const Bicicleta = require('../../models/bicicletas');

exports.bicicleta_list = (req, res) => {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.bicicleta_create = (req, res) => {
    const bici = new Bicicleta( req.body.id, 
                                req.body.color, 
                                req.body.modelo, 
                                [req.body.lat, req.body.lng]
                                );
    Bicicleta.add(bici);

    res.status(200).json({
        bicicletas: bici
    });
}

exports.bicicleta_delete = (req, res) => {
    Bicicleta.deleteById(req.body.id);
    res.status(204).send();
}

exports.bicicleta_update = (req, res) => {
    const bike = Bicicleta.findById(req.body.id);
    bike.id = req.body.id;
    bike.color = req.body.color;
    bike.modelo = req.body.modelo;
    bike.ubicacion = [req.body.lat, req.body.lng];
    res.status(200).json({
        bicicletas: bike
    });
}