const Bicicleta = require("../../models/bicicletas");
const request = require('request');
const server = require('../../bin/www');
const { Mongoose } = require('../../database');
const { ConsoleReporter } = require("jasmine");
//Variables
const api_endpoint = 'http://localhost:3000/api/bicicletas';


describe('Bicicleta API', () => {

    // beforeEach(() => {
    //     // NOTA: al importar el mongoose ya se ejecuta el comando de la bd
    // });

    // //Eliminamos la data creada
    // afterEach((done) => {
    //     Bicicleta.deleteMany({}, (err, success) => {
    //         if (err) console.log(err);
    //         done();
    //     });
    // });

    // describe('Get Bicicletas /', () => {
    //     it('Status 200', () => {
    //         request.get(api_endpoint, (err, response, body) => {
    //             let result = JSON.parse(body);
    //             expect(response.statusCode).toBe(200);
    //             expect(result.bicicletas.length).toBe(0);
    //             done();
    //         });
    //     });
    // });

    // describe('Post Bicicletas /', () => {
    //     it('Status 200', (done) => {
    //         // Definimos el tipo de elemento que enviaremos
    //         let headers = { 'content-type' : 'application/json' };
    //         let bike = { "code": 10, "color": "azul", "modelo": "urbana", "lat": -34, "lng": -54 };
    //         let params = {
    //             "headers": headers,
    //             "url": api_endpoint + "/create",
    //             "body": bike
    //         };
    //         request.post(params, (err, res, body) => {
    //             expect(res.statusCode).toBe(200);
    //             let bike = JSON.parse(body).bicicleta;
    //             console.log(bike);
    //             expect(bike.color).toBe("azul");
    //             expect(bike.ubicacion[0]).toBe(-34);
    //             expect(bike.ubicacion[1]).toBe(-54);
    //             done();
    //         });
    //     });
    // });

    // describe('Put Bicicletas /', () => {
    //     it('Status 200', (done) => {
    //         // Definimos el tipo de elemento que enviaremos
    //         let headers = { 'content-type' : 'application/json' };
    //         let bike = { "id": 10, "color": "dorado", "modelo": "Montaña", "lat": -34, "lng": -54 };
    //         let params = {
    //             "headers": headers,
    //             "url": api_endpoint + '/update',
    //             "body": bike
    //         };
    //         request.put(params, (err, res, body) => {
    //             expect(res.statusCode).toBe(200);
    //             expect(Bicicleta.findById(10).color).toBe("dorado");
    //             done();
    //         });
    //     });
    // });

    // describe('Delete Bicicletas /', () => {
    //     it('Status 200', (done) => {
    //         // Definimos el tipo de elemento que enviaremos
    //         let headers = { 'content-type' : 'application/json' };
    //         let bike = { "id": 10 };
    //         let params = {
    //             "headers": headers,
    //             "url": api_endpoint + '/update',
    //             "body": bike
    //         };
    //         request.delete(params, (err, res, body) => {
    //             expect(res.statusCode).toBe(200);
    //             expect(Bicicleta.allBicis.length).toBe(0);
    //             done();
    //         });
    //     });
    // });

});