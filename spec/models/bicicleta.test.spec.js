const Bicicleta = require('../../models/bicicletas');
const { Mongoose } = require('../../database');

describe('Testing Bicicletas', () => {
    beforeEach(() => {
        // NOTA: Al importar la BD en Mongoose se conecta sin necesidad de estar abriendo conexiones
    });

    //Eliminamos la data creada
    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicletas.createIntance', () => {
        it ('crea una instancia de Bicicletas', () => {
            let bike = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

            expect(bike.code).toBe(1);
            expect(bike.color).toBe("verde");
            expect(bike.modelo).toBe("urbana");
            expect(bike.ubicacion[0]).toEqual(-34.5);
            expect(bike.ubicacion[1]).toEqual(-54.1);
        })
    });

    describe('Bicicletas.allBicis', () => {
        it ('comienza vacia', (done) => {
            Bicicleta.allBicis((err, bike) => {
                expect(bike.length).toBe(0);
                done();
            });
        })
    });

    describe('Bicicletas.add', () => {
        it ('agrego solo una bicicleta', (done) => {
            let bike = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
            Bicicleta.add(bike, (err, newBike) => {
                if (err) console.log(err);
                Bicicleta.allBicis((err, bikes) => {
                    expect(bikes.length).toEqual(1);
                    expect(bikes[0].code).toEqual(bike.code);

                    done();
                });
            });
        })
    });

    describe('Bicicletas.findByCode', () => {
        it ('debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis((err, bikes) => {
                expect(bikes.length).toBe(0);
                
                let bike = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
                Bicicleta.add(bike, (err, newBike) => {
                    if (err) console.log(err);

                    let bike2 = Bicicleta.createInstance(1, "roja", "urbana", [-34.5, -54.1]);
                    Bicicleta.add(bike2, (err, newBike) => {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, (err, targetBike) => {
                            expect(targetBike.code).toBe(bike.code);
                            expect(targetBike.color).toBe(bike.color);
                            expect(targetBike.modelo).toBe(bike.modelo);

                            done();
                        });
                    });
                });

            });
        })
    });
});


// //Se ejecuta antes de cada test.
// beforeEach(() => {
//     // Vaciamos el objeto de bicicletas
//     Bicicleta.allBicis = []
// });

// //describe: testing Group
// describe('Bicicletas.allBicis', () => {
//     it('Comienza vacia', () => {
//         // expect: El dato
//         // toBe: Lo que se espera que sea el dato
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe('Bicicletas.add', () => {
//     it('Agregamos una', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         let bike1 = new Bicicleta(3, "azul", "urbana", [-34.6012424, -58.4061497]);
//         Bicicleta.add(bike1);
//         //Validamos que se agrege
//         expect(Bicicleta.allBicis.length).toBe(1);
//         //Validamos que la bicicleta sea la que agregamos
//         expect(Bicicleta.allBicis[0]).toBe(bike1);
//     });
// });

// describe('Bicicletas.findById', () => {
//     it('Debe devolver la bici con id 1', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         let bike1 = new Bicicleta(3, "verde", "urbana", [-34.7012424, -58.3861497]);
//         let bike2 = new Bicicleta(4, "verde", "urbana", [-34.7012424, -58.4861497]);
//         Bicicleta.add(bike1);
//         Bicicleta.add(bike2);

//         let targetBike = Bicicleta.findById(3);
//         expect(targetBike.id).toBe(3);
//         expect(targetBike.color).toBe("verde");
//         expect(targetBike.modelo).toBe("urbana");
//     });
// });

// describe('Bicicletas.deleteById', () => {
//     it ('Eliminamos una biciclata despues de agregada', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         let bike1 = new Bicicleta(1, "azul", "urbana", [-34.6012424, -58.4061497]);
//         Bicicleta.add(bike1);
//         Bicicleta.deleteById(1);
//         expect(Bicicleta.allBicis.length).toBe(0)
//     });
// });