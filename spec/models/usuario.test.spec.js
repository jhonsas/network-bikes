const mongoose = require('mongoose');
const { Mongoose } = require('../../database');
const Usuario = require('../../models/usuario');
const Bicicleta = require("../../models/bicicletas");
const Reserva = require('../../models/reserva');

describe('Testing Usuarios', () => {
    beforeEach(() => {
        // NOTA: al importar el modulo Mongoose de database ya se tiene la conexión
    });

    //Eliminamos la data creada
    afterEach((done) => {
        Reserva.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            Usuario.deleteMany({}, (err, success) => {
                if (err) console.log(err);
                Bicicleta.deleteMany({}, (err, success) => {
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('Cuando un usuario reserva una bici', () => {
        it('desde existir la reserva', (done) => {
            const usuario = new Usuario({nombre: 'Jhon'});
            usuario.save();
            const bike = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            bike.save();

            let hoy = new Date();
            let manana = new Date();
            manana.setDate(hoy.getDate() + 1);
            usuario.reservar(bike.id, hoy, manana, (err, reserva) => {
                Reserva.find({}).populate('bicicleta').populate('usuario').exec((err, reservas) => {
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(1);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        })
    });

});