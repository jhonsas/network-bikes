const express = require('express');
const router = express.Router();
const usuarioController = require('../controllers/usuarios.controller');

/* GET users listing. */
router.get('/', usuarioController.list);
router.post('/create', usuarioController.create);
router.get('/create', usuarioController.create_get);
router.get('/:id/update', usuarioController.update_get);
router.post('/:id/update', usuarioController.update);
router.post('/:id/delete', usuarioController.delete);

module.exports = router;
